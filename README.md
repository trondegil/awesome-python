# awesome python

A curated list of awesome Python frameworks, libraries, software and resources.

Inspired by [vinta/awesome-python] (https://github.com/vinta/awesome-python)

# Index
- [Awesome Python](#awesome-python)
    - [Command-line](#command-line)



---
## ASGI Servers

*[ASGI](https://asgi.readthedocs.io/en/latest/)-compatible web servers.*

* [uvicorn](https://github.com/encode/uvicorn) - A lightning-fast ASGI server implementation, using uvloop and httptools.

## Authentication

*Libraries for implementing authentications schemes.*

* [pyjwt](https://github.com/jpadilla/pyjwt) - JSON Web Token implementation in Python.

## Build Tools
*Compile software from source code.*

* [pybuilder](https://github.com/pybuilder/pybuilder) - A continuous build tool written in pure Python.

## Built-in Classes Enhancement

*Libraries for enhancing Python built-in classes.*

* [DottedDict](https://github.com/carlosescri/DottedDict) - A library that provides a method of accessing lists and dicts with a dotted path notation.
## Code Analysis
*Tools of linters and code formaters. Also see [awesome-static-analysis](https://github.com/mre/awesome-static-analysis).*

* Code Linters
    * [flake8](https://pypi.org/project/flake8/) - A wrapper around pycodestyle, pyflakes and McCabe.
        * [awesome-flake8-extensions](https://github.com/DmytroLitvinov/awesome-flake8-extensions)
    * [pylint](https://www.pylint.org/) - A fully customizable source code analyzer.
* Code Formatters
    * [black](https://github.com/python/black) - The uncompromising Python code formatter.
## Command-line
*Tools to conqure the command line*

* Command-line Application Development
    * [Typer](https://typer.tiangolo.com/) - Typer, build great CLIs. Easy to code. Based on Python type hints.
    * [click](http://click.pocoo.org/dev/) - A package for creating beautiful command line interfaces in a composable way.
* CLI Enhancements
    * [httpie](https://github.com/jakubroztocil/httpie) - A command line HTTP client, a user-friendly cURL replacement.
* Terminal Rendering
    * [rich](https://github.com/willmcgugan/rich) - Python library for rich text and beautiful formatting in the terminal. Also provides a great RichHandler log handler.
* Productivity Tools
    * [copier](https://github.com/pykong/copier) - A library and command-line utility for rendering projects templates.
    * [cookiecutter](https://github.com/audreyr/cookiecutter) - A command-line utility that creates projects from cookiecutters (project templates).

## Data Analysis

*Libraries for data analyzing.*
* [Pandas](http://pandas.pydata.org/) - A library providing high-performance, easy-to-use data structures and data analysis tools.

## Data Validation

*Libraries for validating data. Used for forms in many cases.*

* [pydantic](https://github.com/samuelcolvin/pydantic/) - Data validation and settings management using python type annotations. [(docs)](https://pydantic-docs.helpmanual.io/)

## Data Visualization

*Libraries for visualizing data. Also see [awesome-javascript](https://github.com/sorrycc/awesome-javascript#data-visualization).*

* [diagrams](https://github.com/mingrammer/diagrams) - Diagram as Code.
* [Matplotlib](http://matplotlib.org/) - A Python 2D plotting library.
* [Pygal](http://www.pygal.org/en/latest/) - A Python SVG Charts Creator.
* [PyGraphviz](https://pypi.org/project/pygraphviz/) - Python interface to [Graphviz](http://www.graphviz.org/).
* [Seaborn](https://github.com/mwaskom/seaborn) - Statistical data visualization using Matplotlib.

## DevOps Tools
*Software and libraries for DevOps.*

* [Fabric](https:/https://github.com/fabric/fabric/) - A simple, Pythonic tool for remote execution and deployment. [(docs)](https://www.fabfile.org/)

## Files
*Libraries for file manipulation.*

* [pathlib](https://docs.python.org/3/library/pathlib.html) - (Python standard library) An cross-platform, object-oriented path library.  [(docs)](https://docs.python.org/3/library/pathlib.html) 
* [shutil](https://docs.python.org/3/library/shutil.html) - (Python standard library) The shutil module offers a number of high-level operations on files and collections of files.  [(docs)](https://docs.python.org/3/library/shutil.html) 


## Convert / Download html
* [html2text](https://github.com/Alir3z4/html2text) - Convert a page of HTML into clean, easy-to-read plain ASCII text. [(docs)](https://github.com/Alir3z4/html2text/blob/master/docs/usage.md) 
* [requests-html](https://github.com/psf/requests-html) - Make parsing HTML as simple and intuitive as possible. [(docs)](https://docs.python-requests.org/projects/requests-html/en/latest/) 
* [lassie](https://github.com/michaelhelmick/lassie) - Lassie is a Python library for retrieving basic content from websites. [(docs)](https://lassie.readthedocs.io/en/latest/) 


## Database / ORM
*Libraries that implement Object-Relational Mapping or data mapping techniques.*

* [SQLAlchemy](https://github.com/sqlalchemy/sqlalchemy)  - The Python SQL Toolkit and Object Relational Mapper. [(docs)](https://www.sqlalchemy.org/) 
* [mongoengine](https://github.com/MongoEngine/mongoengine)  - A Python Object-Document-Mapper for working with MongoDB. [(docs)](https://mongoengine-odm.readthedocs.io/) 
* [PynamoDB](https://github.com/pynamodb/PynamoDB) - A Pythonic interface for Amazon DynamoDB. [(docs)]( https://pynamodb.readthedocs.io/) 

## Date and Time
*Libraries for working with dates and times.*
* [dateutil](https://github.com/dateutil/dateutil) - Extensions to the standard Python datetime module. [(docs)](https://dateutil.readthedocs.io/en/stable/) 
* [arrow](https://github.com/arrow-py/arrow) - Better dates & times for Python [(docs)](https://arrow.readthedocs.io/en/latest/) 
* [DateTimeRange](https://github.com/thombashi/DateTimeRange) - Handle a time range. e.g. check whether a time is within the time range, get the intersection of time ranges etc. [(docs)](https://datetimerange.readthedocs.io/en/latest/) 

## RESTful API
*Libraries for building RESTful APIs.*

* [FastAPI](https://github.com/tiangolo/fastapi) - A modern, fast, web framework for building APIs with Python 3.6+ based on standard Python type hints.  [(docs)](https://fastapi.tiangolo.com/) 


## Specific Formats Processing

*Libraries for parsing and manipulating specific text formats.*

* General
    * [tablib](https://github.com/jazzband/tablib) - A module for Tabular Datasets in XLS, CSV, JSON, YAML.
* Office
    * [docxtpl](https://github.com/elapouya/python-docx-template) - Editing a docx document by jinja2 template
    * [openpyxl](https://openpyxl.readthedocs.io/en/stable/) - A library for reading and writing Excel 2010 xlsx/xlsm/xltx/xltm files.
    * [python-docx](https://github.com/python-openxml/python-docx) - Reads, queries and modifies Microsoft Word 2007/2008 docx files.
    * [python-pptx](https://github.com/scanny/python-pptx) - Python library for creating and updating PowerPoint (.pptx) files.
    * [unoconv](https://github.com/unoconv/unoconv) - Convert between any document format supported by LibreOffice/OpenOffice.
    * [XlsxWriter](https://github.com/jmcnamara/XlsxWriter) - A Python module for creating Excel .xlsx files.
    * [xlwt](https://github.com/python-excel/xlwt) / [xlrd](https://github.com/python-excel/xlrd) - Writing and reading data and formatting information from Excel files.
* PDF
    * [PyPDF2](https://github.com/mstamy2/PyPDF2) - A library capable of splitting, merging and transforming PDF pages.
    * [ReportLab](https://www.reportlab.com/opensource/) - Allowing Rapid creation of rich PDF documents.
    * [pikepdf](https://github.com/pikepdf/pikepdf) - Python library for reading and writing PDF files.
* Markdown
    * [Mistune](https://github.com/lepture/mistune) - Fastest and full featured pure Python parsers of Markdown.
    * [Python-Markdown](https://github.com/waylan/Python-Markdown) - A Python implementation of John Gruber’s Markdown.
* YAML
    * [PyYAML](http://pyyaml.org/) - YAML implementations for Python.
* CSV
    * [csvkit](https://github.com/wireservice/csvkit) - Utilities for converting to and working with CSV.


## Static Site Generator
*Static site generator is a software that takes some text + templates as input and produces HTML files on the output.*

* [mkdocs](https://github.com/mkdocs/mkdocs/) - Markdown friendly documentation generator.


## Template Engine

*Libraries and tools for templating and lexing.*
* [Jinja2](https://github.com/pallets/jinja) - A modern and designer friendly templating language.

## Testing
*Libraries for testing codebases and generating test data.*

* Testing framework
    * [pytest](https://github.com/pytest-dev/pytest/) - A mature full-featured Python testing tool. [(docs)](https://docs.pytest.org/en/latest/)

* Fake Data
    * [mimesis](https://github.com/lk-geimfari/mimesis) - is a Python library that help you generate fake data. [(docs)](https://mimesis.name/)
    * [faker](https://github.com/joke2k/faker) - A Python package that generates fake data. [(docs)](https://faker.readthedocs.io/en/stable/)
# Resources
## Websites
